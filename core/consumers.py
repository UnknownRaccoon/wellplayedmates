import json
from channels.channel import Group
from channels.sessions import channel_session
from django.contrib.auth.models import User
from django.db.models.aggregates import Count
from core.models import ChatMessage, Chat

connections = dict()

@channel_session
def ws_add(message):
    query_string = message.content['query_string']
    query_dict = dict([param.split('=') for param in query_string.split('&')])
    current_user = User.objects.get(pk=query_dict['userID'])
    connections[str(message.reply_channel)] = current_user.id
    message.channel_session['reply_channel'] = current_user.id
    for chat in current_user.chats.all():
        Group(str(chat.id)).add(message.reply_channel)
    message.reply_channel.send({"accept": True})

@channel_session
def ws_message(message):
    data = json.loads(message.content['text'])
    user_id = data['data']['userID']
    if data['type'] == 'message':
        message_text = data['data']['message']
        chat_id = data['data']['channel']
        ChatMessage.objects.create(text=message_text, sender_id=user_id, chat_id=chat_id)
        Group(str(data['data']['channel'])).send({
            "text": json.dumps({
                'type': 'message',
                'data': {
                    'message': message_text,
                    'channel': chat_id,
                    'userID': user_id
                }})})
    elif data['type'] == 'create':
        users_ids = data['data']['userIDs']
        chats = Chat.objects.annotate(c=Count('users')).filter(c=len(users_ids))
        for user in users_ids:
            chats = chats.filter(users__id=user)
        if len(chats) == 0:
            chat = Chat.objects.create()
            chat.users.add(*users_ids)
        else:
            chat = chats.first()
        chat_users = chat.users.exclude(id=user_id)

        for key, value in connections.items():
            if value in users_ids:
                Group(str(chat.id)).add(key)
        Group(str(chat.id)).send({
            "text": json.dumps({
                "type": "create",
                "data": {
                    "users": [{'id': user.id, 'name': user.username} for user in chat_users],
                    "chatID": chat.id
                }
            })
        })


@channel_session
def ws_disconnect(message):
    del connections[str(message.reply_channel)]
