from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from core.models import ChatMessage


@receiver(post_save, sender=ChatMessage)
def create_message_status(sender, **kwargs):
    if kwargs['created'] == True:
        message = kwargs['instance']
        for user in message.chat.users.all():
            user.messageuserstatus_set.create(message=message)
