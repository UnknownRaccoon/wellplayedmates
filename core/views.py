from django.views.generic.base import TemplateView, TemplateResponseMixin

from games.models import Game
from news.models import NewsItem
from teams.models import Team
from tournaments.models import Tournament


class WpViewMixin(object):
    @property
    def seo_description(self):
        raise NotImplementedError

    @property
    def seo_image(self):
        raise NotImplementedError

    @property
    def page_title(self):
        raise NotImplementedError

    def get_context_data(self, *args, **kwargs):
        context = super(WpViewMixin, self).get_context_data(*args, **kwargs)
        context['page_title'] = self.page_title
        return context


class MainPageView(WpViewMixin, TemplateView):
    template_name = 'core/main-page.html'

    def page_title(self):
        return 'WpMates - main page'

    def get_context_data(self, **kwargs):
        context = super(MainPageView, self).get_context_data(**kwargs)
        context['featured_games'] = Game.objects.filter(is_featured=True)
        context['featured_teams'] = Team.objects.order_by('-level')[:4]
        context['featured_tournaments'] = Tournament.objects.order_by('-id')[:3]
        context['featured_news'] = NewsItem.objects.order_by('-id')[:4]
        return context
