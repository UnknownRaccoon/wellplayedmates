from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.db import models
from django.contrib.auth.models import User
from games.models import Game


class SiteSettings(models.Model):
    site = models.OneToOneField(Site)


class Tag(models.Model):
    name = models.CharField(max_length=255)


class Chat(models.Model):
    users = models.ManyToManyField(User, related_name='chats')

    def __str__(self):
        return str(self.id)


class ChatMessage(models.Model):
    text = models.CharField(max_length=255)
    sender = models.ForeignKey(User)
    chat = models.ForeignKey(Chat, related_name='messages')
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Sender: {self.sender}, text: "{self.text[:100]}"'


class MessageUserStatus(models.Model):
    user = models.ForeignKey(User)
    is_read = models.BooleanField(default=False)
    message = models.ForeignKey(ChatMessage)


class Comment(models.Model):
    author = models.ForeignKey(User)
    date_created = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    commented_item = GenericForeignKey()


class Notification(models.Model):
    sender_id = models.PositiveIntegerField()
    content_type = models.ForeignKey(ContentType)
