from django.conf.urls import url

from core.views import MainPageView

urlpatterns = [
    url(r'^$', MainPageView.as_view(), name='main-page'),
]
