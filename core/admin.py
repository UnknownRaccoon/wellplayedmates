from django.contrib import admin

from core.models import Chat, ChatMessage, MessageUserStatus, Comment


class ChatAdmin(admin.ModelAdmin):
    filter_horizontal = ('users',)


class ChatMessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'text', 'date_created')


admin.site.register(Chat, ChatAdmin)
admin.site.register(ChatMessage, ChatMessageAdmin)
admin.site.register(MessageUserStatus)
admin.site.register(Comment)
