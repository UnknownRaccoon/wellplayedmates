from django.contrib.auth import login
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.http.response import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from rest_framework_jwt.serializers import jwt_payload_handler
from rest_framework_jwt.utils import jwt_encode_handler

from core.views import WpViewMixin
from wellplayedmates.helpers import JsonError


class RegisterView(TemplateView, WpViewMixin):
    template_name = 'user_management/registration.html'

    def page_title(self):
        return 'WpMates - Registration'


class LoginView(TemplateView, WpViewMixin):
    template_name = 'user_management/login.html'

    def page_title(self):
        return 'WpMates - Registration'