from django.contrib.auth.models import User as DjangoUser
from django.db.models.signals import pre_save, post_save
from django.dispatch.dispatcher import receiver
from user_management.models import User


@receiver(pre_save, sender=User)
def user_update(sender, **kwargs):
    user = kwargs['instance']
    if user.id:
        user = User.objects.get(id=user.id)
        storage, path = user.avatar.storage, user.avatar.path
        storage.delete(path)

@receiver(post_save, sender=DjangoUser)
def create_message_status(sender, **kwargs):
    if kwargs['created'] == True:
        django_user = kwargs['instance']
        User.objects.get_or_create(basic_user_id=django_user.id)
