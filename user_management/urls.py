from django.conf.urls import url

from user_management.views import RegisterView, LoginView

urlpatterns = [
    url(r'^register/$', RegisterView.as_view(), name='register'),
    url(r'^login/$', LoginView.as_view(), name='login'),
]
