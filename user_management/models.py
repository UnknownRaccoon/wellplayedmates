import os

from django.conf import settings
from django.contrib.auth.models import User as DjangoUser
from django.db import models

from wellplayedmates.helpers import get_safe_str


class User(models.Model):
    def avatar_path(self, file_name):
        return os.path.join('users', get_safe_str(self.basic_user.username), 'avatar.' + file_name.split('.')[-1])

    basic_user = models.OneToOneField(DjangoUser, related_name='user_details')
    friends = models.ManyToManyField('self', blank=True)
    avatar = models.ImageField(upload_to=avatar_path, default=os.path.join(settings.MEDIA_ROOT, 'img', 'user.png'))

    @property
    def dict(self):
        return {'image': self.avatar.url}
