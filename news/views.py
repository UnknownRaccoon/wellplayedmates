from django.db.models.query_utils import Q
from django.views.generic.detail import DetailView
from core.views import WpViewMixin
from news.models import NewsItem


class NewsItemDetailsView(WpViewMixin, DetailView):
    model = NewsItem

    def page_title(self):
        return self.object.title

    def get_context_data(self, *args, **kwargs):
        context = super(NewsItemDetailsView, self).get_context_data(*args, **kwargs)
        context['nearest_news'] =  list(NewsItem.objects.filter(id__gt=self.object.id)[:3]) + list(NewsItem.objects.filter(id__lt=self.object.id)[:2])
        return context
