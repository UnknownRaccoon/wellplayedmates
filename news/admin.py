from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from news.models import NewsItem


class NewsMultilangAdmin(TranslationAdmin):
    pass

admin.site.register(NewsItem, NewsMultilangAdmin)
