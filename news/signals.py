from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from news.models import NewsItem


@receiver(pre_save, sender=NewsItem)
def news_item_update(sender, **kwargs):
    item = kwargs['instance']
    if item.id:
        item = NewsItem.objects.get(id=item.id)
        storage, path = item.background.storage, item.background.path
        storage.delete(path)
