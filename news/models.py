import os
from datetime import datetime

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage


from django.conf.urls.static import static
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from core.models import Comment
from games.models import Game
from teams.models import Team


class NewsItem(models.Model):
    def background_path(self, file_name):
        return os.path.join('news', str(datetime.now()) + file_name.split('.')[-1])


    title = models.CharField(max_length=255)
    text = models.TextField()
    author = models.ForeignKey(User, null=True, blank=True)
    related_games = models.ManyToManyField(Game, related_name='news', blank=True)
    related_teams = models.ManyToManyField(Team, related_name='news', blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    likes = models.PositiveIntegerField(default=0)
    comments = GenericRelation(Comment)
    background = models.ImageField(upload_to=background_path)

    @property
    def author_avatar_url(self):
        if not self.author:
            return staticfiles_storage.url(settings.STATIC_LOGO_URL)
        return self.author.user_details.avatar.url

    def __str__(self):
        return self.title
