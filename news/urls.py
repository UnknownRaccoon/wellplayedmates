from django.conf.urls import url
from news.views import NewsItemDetailsView

urlpatterns = [
    url(r'^(?P<pk>[-\w]+)/$', NewsItemDetailsView.as_view(), name='news-detail'),
]
