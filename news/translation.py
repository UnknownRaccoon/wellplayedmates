from modeltranslation.translator import translator, TranslationOptions

from news.models import NewsItem


class GamesTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)

translator.register(NewsItem, GamesTranslationOptions)
