from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from invitations.models import Request
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_jwt.serializers import jwt_payload_handler
from rest_framework_jwt.utils import jwt_encode_handler
from teams.models import Team


class UserRegistrationSerializer(serializers.HyperlinkedModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=User.objects.all())])
    passwordConfirmation = serializers.CharField(max_length=255, write_only=True)
    token = serializers.SerializerMethodField('user_token')
    details = serializers.SerializerMethodField('user_details')

    def user_token(self, user_object):
        return jwt_encode_handler(jwt_payload_handler(user_object))

    def user_details(self, user_object):
        return user_object.user_details.dict

    def validate(self, attrs):
        if attrs['password'] != attrs['passwordConfirmation']:
            raise serializers.ValidationError({'passwordConfirmation': "Passwords don't match"})
        attrs['password'] = make_password(attrs['password'])
        del attrs['passwordConfirmation']
        return attrs

    class Meta:
        model = User
        fields = ('id', 'username', 'password', 'email', 'passwordConfirmation', 'token', 'details')
        extra_kwargs = {
            'password': {'write_only': True},
            'id': {'read_only': True},
        }


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'


class InvitationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Request
        fields = '__all__'
