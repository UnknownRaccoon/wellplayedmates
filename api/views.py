import json

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.http.response import JsonResponse
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework_jwt.serializers import jwt_encode_handler, jwt_payload_handler

from api.serializers import UserRegistrationSerializer, TeamSerializer
from teams.models import Team
from wellplayedmates.helpers import JsonError


class UsersListView(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserRegistrationSerializer


class UserLoginView(APIView):
    def post(self, request, *args, **kwargs):
        user_info = json.loads(request.body)
        try:
            user = authenticate(request, username=user_info['username'], password=user_info['password'])
        except KeyError as error:
            return JsonError({error.args[0]: 'The field is required'}, status=409)
        try:
            user_dict = model_to_dict(user, fields=('id', 'username', 'email'))
        except AttributeError:
            return JsonError('User with specified credentials does not exist', status=404)
        user_dict['details'] = user.user_details.dict
        user_dict.update({'token': jwt_encode_handler(jwt_payload_handler(user))})
        return JsonResponse(user_dict)


class TeamsListView(ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TeamSerializer
    def get_queryset(self):
        return Team.objects.all()


class InvitationListView(ListCreateAPIView):
    permission_classes = (IsAuthenticated, )
    # serializer_class = Invita