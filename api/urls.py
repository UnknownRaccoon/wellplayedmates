from django.conf.urls import url
from api.views import UsersListView, UserLoginView, TeamsListView

urlpatterns = [
    url(r'^users/$', UsersListView.as_view(), name='users-list'),
    url(r'^sessions/$', UserLoginView.as_view(), name='login'),
    url(r'^teams/$', TeamsListView.as_view(), name='teams'),
    url(r'^games/$', TeamsListView, name='games'),
    url(r'^games/(?P<game_id>\d+)/$', TeamsListView, name='game'),
    url(r'^games/(?P<game_id>\d+)/teams/$', TeamsListView, name='game-teams'),
]
