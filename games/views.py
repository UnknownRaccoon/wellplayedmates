from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from core.views import WpViewMixin
from games.models import Game


class GamesListView(WpViewMixin, ListView):
    model = Game
    queryset = Game.objects.all()

    def page_title(self):
        return 'Games List'


class GameDetailView(WpViewMixin, DetailView):
    model = Game

    def page_title(self):
        return self.object.name
