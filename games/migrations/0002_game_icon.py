# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-18 14:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='icon',
            field=models.ImageField(default='', upload_to='s'),
            preserve_default=False,
        ),
    ]
