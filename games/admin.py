from django.contrib import admin
from modeltranslation.admin import TranslationAdmin
from games.models import Game


class GameMultilangAdmin(TranslationAdmin):
    pass

admin.site.register(Game, GameMultilangAdmin)
