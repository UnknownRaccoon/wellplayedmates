from django.conf.urls import url
from games.views import GamesListView, GameDetailView

urlpatterns = [
    url(r'^$', GamesListView.as_view(), name='games-list'),
    url(r'^(?P<pk>[-\w]+)/$', GameDetailView.as_view(), name='game-detail'),
]
