from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from games.models import Game


@receiver(pre_save, sender=Game)
def game_update(sender, **kwargs):
    game = kwargs['instance']
    if game.id:
        game = Game.objects.get(id=game.id)
        storage, path = game.icon.storage, game.icon.path
        storage.delete(path)
        storage, path = game.background.storage, game.background.path
        storage.delete(path)
