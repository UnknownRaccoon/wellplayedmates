from modeltranslation.translator import translator, TranslationOptions
from games.models import Game


class GamesTranslationOptions(TranslationOptions):
    fields = ('description',)

translator.register(Game, GamesTranslationOptions)
