import os
import requests
from django.contrib.auth.models import User
from django.db import models


from wellplayedmates.helpers import get_safe_str


class Game(models.Model):
    def icon_path(self, file_name):
        return os.path.join('games', get_safe_str(self.name), 'icon.' + file_name.split('.')[-1])

    def background_path(self, file_name):
        return os.path.join('games', get_safe_str(self.name), 'background.' + file_name.split('.')[-1])

    name = models.CharField(max_length=255)
    description = models.TextField()
    icon = models.ImageField(upload_to=icon_path)
    is_featured = models.BooleanField(default=False)
    background = models.ImageField(upload_to=background_path)

    def update_gamers_data(self):
        for gamer in self.usergamerelation_set.all():
            gamer.data = requests.get(f'https://owapi.net/api/v3/u/{gamer.user_game_id}/blob',
                                      headers={'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'}).content
            gamer.save()

    def __str__(self):
        return self.name


class UserGameRelation(models.Model):
    user = models.ForeignKey(User)
    game = models.ForeignKey(Game)
    user_game_id = models.CharField(max_length=255)
    data = models.TextField(null=True, blank=True)
