import re

from django.http.response import JsonResponse


def get_safe_str(input_string):
    safe_str_pattern = re.compile('[\W_]+', re.UNICODE)
    return safe_str_pattern.sub('', input_string)


class JsonError(JsonResponse):
    def __init__(self, error, *args, **kwargs):
        super(JsonError, self).__init__({'error': error}, *args, **kwargs)
