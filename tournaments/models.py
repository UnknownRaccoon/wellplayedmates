import os
from django.db import models

from games.models import Game
from wellplayedmates.helpers import get_safe_str


class Company(models.Model):
    name = models.CharField(max_length=255)
    link = models.URLField()


class Tournament(models.Model):
    def image_path(self, file_name):
        return os.path.join('tournaments', get_safe_str(self.name), 'image.' + file_name.split('.')[-1])

    name = models.CharField(max_length=255)
    description = models.TextField()
    starts_at = models.DateTimeField()
    ends_at = models.DateTimeField()
    company = models.ForeignKey(Company)
    image = models.ImageField(upload_to=image_path)
    game = models.ForeignKey(Game)
