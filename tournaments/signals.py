from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from tournaments.models import Tournament


@receiver(pre_save, sender=Tournament)
def tournament_update(sender, **kwargs):
    tournament = kwargs['instance']
    if tournament.id:
        tournament = Tournament.objects.get(id=tournament.id)
        storage, path = tournament.image.storage, tournament.image.path
        storage.delete(path)
