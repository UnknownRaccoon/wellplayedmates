from django.contrib import admin

from tournaments.models import Company, Tournament

admin.site.register(Company)
admin.site.register(Tournament)
