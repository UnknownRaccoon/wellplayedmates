# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-20 17:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('games', '0005_game_background'),
        ('tournaments', '0002_tournament_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='tournament',
            name='game',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='games.Game'),
            preserve_default=False,
        ),
    ]
