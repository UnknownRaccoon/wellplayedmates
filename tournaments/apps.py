from django.apps import AppConfig


class TournamentsConfig(AppConfig):
    name = 'tournaments'

    def ready(self):
        from tournaments import signals
