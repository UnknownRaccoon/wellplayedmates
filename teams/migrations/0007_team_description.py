# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-18 19:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0006_auto_20170318_1941'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='description',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
