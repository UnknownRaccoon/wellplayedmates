from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver
from teams.models import Team


@receiver(pre_save, sender=Team)
def team_update(sender, **kwargs):
    team = kwargs['instance']
    if team.id:
        team = Team.objects.get(id=team.id)
        storage, path = team.image.storage, team.image.path
        storage.delete(path)
