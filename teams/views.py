from django.views.generic.detail import DetailView

from core.views import WpViewMixin
from teams.models import Team


class TeamDetailsView(WpViewMixin, DetailView):
    model = Team
