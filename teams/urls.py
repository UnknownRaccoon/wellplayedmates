from django.conf.urls import url
from teams.views import TeamDetailsView

urlpatterns = [
    url(r'^(?P<pk>[-\w]+)/$', TeamDetailsView.as_view(), name='team-detail'),
]