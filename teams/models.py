import os
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

from games.models import Game
from wellplayedmates.helpers import get_safe_str


class GamesTeamsRelation(models.Model):
    game = models.ForeignKey(Game)
    team = models.ForeignKey('Team')
    is_leader = models.BooleanField(default=False)


class Team(models.Model):
    def image_path(self, file_name):
        return os.path.join('teams', get_safe_str(self.name), 'image.' + file_name.split('.')[-1])

    members = models.ManyToManyField(User)
    leader = models.ForeignKey(User, related_name='created_teams')
    name = models.CharField(max_length=255, unique=True)
    likes = models.PositiveIntegerField(default=0)
    level = models.PositiveIntegerField(default=0)
    description = models.TextField()
    games = models.ManyToManyField(Game, related_name='teams', through=GamesTeamsRelation)
    image = models.ImageField(upload_to=image_path, default=os.path.join(settings.MEDIA_ROOT, 'img', 'team.png'))
